from . import CustomizedException
from http import HTTPStatus

class ConnectionFailException(CustomizedException):
    """
        An error class if there is no connection to the destination url 
    """
    def __init__(self, message: str):
            CustomizedException.__init__(self, message, HTTPStatus.BAD_GATEWAY)


class CompanyNotFound(CustomizedException):
    """
        Error class if it is not possible to create a ticket with yfiance 
    """ 
    def __init__(self, message: str):
            CustomizedException.__init__(self, message, HTTPStatus.NOT_FOUND)