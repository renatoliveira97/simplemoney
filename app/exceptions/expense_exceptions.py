from http import HTTPStatus
from . import CustomizedException


class InvalidCategoryException(CustomizedException):
    """
        Exception class if has invalid category in the
        data.
    """
    def __init__(self, message: str):
            CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)
