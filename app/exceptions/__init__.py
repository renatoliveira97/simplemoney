from http import HTTPStatus


class CustomizedException(Exception):

    def __init__(self, message: str, code: HTTPStatus):
        self.message = message
        self.code = code

    def get_message(self) -> str:
        return self.message

    def get_status_code(self) -> HTTPStatus:
        return self.code
