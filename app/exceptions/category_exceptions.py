from http import HTTPStatus
from . import CustomizedException


class CategoryNotFoundException(CustomizedException):
    """
        Exception class for a category that doesn't exist.
    """
    def __init__(self, message):
        CustomizedException.__init__(self, message, HTTPStatus.NOT_FOUND)
 