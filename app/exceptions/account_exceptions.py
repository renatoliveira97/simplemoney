from . import CustomizedException
from http import HTTPStatus


class AccountAlreadyExists(CustomizedException):
    """
        Exception class for an account that already exist.
    """
    def __init__(self, message):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)
