from . import CustomizedException
from http import HTTPStatus


class FormatErrorException(CustomizedException):
    """
       Exeption class for wrong format value
    """

    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)


class InvalidFieldType(CustomizedException):
    """
       Exeption class for invalid type of a field
    """

    def __init__(self, message):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)  


class ValueAlreadyInUseException(CustomizedException):
    """
       Exeption class for a value already in use
    """

    def __init__(self, message: str):
            CustomizedException.__init__(self, message, HTTPStatus.CONFLICT)


class InvalidTypeException(CustomizedException):
    """
        Exception class if has invalid types in the 
        data provided by the request data.
    """
    def __init__(self, message: str):
            CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)


class InvalidValueException(CustomizedException):
    """
        Exception class if has invalid values in
        the keys of the provided data.
    """
    def __init__(self, message: str):
            CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)


class MissingKeyException(CustomizedException):
    """
        Exception class if has missing keys in the
        data provided by the request data.
    """
    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)    


class InvalidKeyException(CustomizedException):
    """
        Exception class if has invalid keys in the
        data provided by the request data.
    """
    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)


class InvalidTotalOfKeysException(CustomizedException):

    """
        Exception class if has only one valid key in the
        data provided by the request data.
    """
    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)
            