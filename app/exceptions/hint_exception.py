from . import CustomizedException
from http import HTTPStatus


class HintNotFoundException(CustomizedException):

    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.NOT_FOUND)
