from http import HTTPStatus
from . import CustomizedException


class NotFoundUserError(CustomizedException):
    """
       Exeption class for user not found in the 
       database. 
    """

    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.NOT_FOUND)


class InvalidRoleException(CustomizedException):
    """
       Exeption class for invalid user inputed 
    """

    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.NOT_FOUND)
