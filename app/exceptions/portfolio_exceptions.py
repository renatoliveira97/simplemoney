from http import HTTPStatus
from . import CustomizedException


class PortfolioAlreadyExist(CustomizedException):
    """
        Exception class if the portfolio
        name already exist.
    """
    def __init__(self, message: str):
        CustomizedException.__init__(self, message, code = HTTPStatus.CONFLICT)
    
class PortfolioDoesNotExist(CustomizedException):
    """
        Exception class if the portfolio
        name doesn't exist.
    """
    def __init__(self, message: str):
        CustomizedException.__init__(self, message, code = HTTPStatus.NOT_FOUND)

class MissingKeyException(CustomizedException):
    """
        Exception class if has missing keys in the
        data provided by the request data.
    """
    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)


class InvalidKeyException(CustomizedException):
    """
        Exception class if has missing keys in the
        data provided by the request data.
    """
    def __init__(self, message: str):
        CustomizedException.__init__(self, message, HTTPStatus.BAD_REQUEST)

