hints_list: list = [
    {
        "title": "PFP: Consumer",
        "body": "Você é uma pessoa que deseja ter mais recursos para poder consumir no presente, por isso o ideal para você é que foque a maior parte dos teus recursos em gastos essenciais, cerca de 40% em lazer e entretenimento, e o resto em investimentos de baixo risco para criar uma reserva de segurança.",
        "type": "Personal Finance"
    },
    {
        "title": "PFP: Enthusiastic",
        "body": "Você preza por sempre estar atualizado em relação aos seus bens, e a grande diferença de um entusiasta para um consumidor é de que você possui um maior conhecimento sobre a gestão de seus recursos, e é por este motivo que o ideal é que você tente gerenciar seus recursos essenciais focando em gastar o mínimo possível sem perder qualidade de vida, e separe ao menos 10% dos seus ganhos para investimentos futuros.",
        "type": "Personal Finance"
    },
    {
        "title": "PFP: Investor",
        "body": "Você é uma pessoa que preza por guardar recursos para poder consumir mais no futuro, ou até mesmo para montar sua aposentadoria. Para você é importante que haja o menor desperdício possível ao longo do mês para que sobre mais recursos para investir, e apesar de haver diferentes tipos de perfis de investidor, o interessante é que não invista muito mais que 10% da sua receita, pois investir não resume apenas na geração de recursos financeiros, mas também se refere a geração de valor e bem estar.",
        "type": "Personal Finance"
    },
    {
        "title": "PFP: Entrepreneur",
        "body": "Diz-se daquele que deseja focar o resíduo de suas receitas na construção de valor no longo prazo por meio de negócios próprios, como por exemplo trabalho autônomo e a construção de uma empresa. Para este perfil, o ideal é gastar suas reservas na sua profissionalização e na compra de bens e recursos com capacidade de geração de valor dentro da área de seu conhecimento.",
        "type": "Personal Finance"
    },
    {
        "title": "IP: Conservative",
        "body": "Você é uma pessoa que apesar de desejar ter maiores ganhos, o seu foco está em assumir o menor risco possível, se sujeitando a maiores riscos apenas quando o retorno mais do que justificar esse aumento. O ideal para este perfil de investidor, é que mais da metade de sua carteira seja formada por ativos de risco quase zero, enquanto a parte restante é formada por investimentos de baixo risco.",
        "type": "Investments"
    },
    {
        "title": "IP: Moderate",
        "body": "Investidores moderados são aqueles que estão dispostos a correr mais riscos para obter melhores resultados. Você é uma pessoa que não consegue viver vendo seus investimentos rendendo tão pouco, por isso acaba buscando por meios de rentabilizar mais, o que pode ser bom ou ruim dependendo do seu conhecimento sobre esses bens. O ideal para este tipo de investidor é de que a segunda parte de sua carteira seja formada por um mix de ativos de baixo e médio risco, fazendo assim com que seu retorno aumente considerávelmente no longo prazo.",
        "type": "Investments"
    },
    {
        "title": "IP: Risk Prone",
        "body": "Propenso ao risco não é o mesmo que correr riscos sempre de forma não planejada, a verdade é que pessoas que possuem este perfil muitas vezes não buscam apenas maiores retornos financeiros, pois a adrenalina de poder ganhar 500 vezes mais ou simplesmente perder tudo, gera na pessoa uma sensação de extâse, o que pode ser extremamente recompensador, e caso isso seja unido a boas estratégias de investimentos, é possível obter ganhos imensos tanto no longo quanto no curto prazo. Apesar disso tudo, sempre esteja atento para não acabar tratando seus investimentos como um jogo de azar, e jamais concentre a maior parte dos seus recursos em investimentos de alto risco, pois o ideal é que você tenha uma boa reserva para caso haja perdas severas de capital.",
        "type": "Investments"
    }
]