import os
from typing import Literal
from app.configs.database import db
from dataclasses import dataclass
from datetime import datetime
from time import time
import jwt
import random, string
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.orm import relationship, validates
import re
from app.exceptions.user_exceptions import (
    NotFoundUserError,
    InvalidRoleException
)
from app.exceptions.expense_exceptions import (
    InvalidCategoryException
)
from app.exceptions.public_exceptions import (
    FormatErrorException,
    InvalidFieldType,
    MissingKeyException,
    InvalidKeyException,
    ValueAlreadyInUseException,
    InvalidTotalOfKeysException
)

@dataclass
class UserModel(db.Model):
    """
        Class that represents the user with their 
        personal information

        ...

        Attributes
        ----------
        name : `str`
            the user's name

        cpf : `str`
            the user's cpf

        email : `str`
            the user's email

        phone : `str`
            the user's phone

        balance : `float`
            the user's account balance
        
        role : `str`
            the user's role. Can be member or admin

        subscription : `str`
            the user's subscription. Can be free, MEI, 
            small business or enterprise

        image_url : `str`
            the user's image url
        
        created_at : `datetime`
            the date and time that the user was created

        Methods
        -------
        password (self, password_to_hash: `str`)
            Class setter method that saves the user's 
            password as a hash in the database.

        verify_password (self, password_to_compare: `str`)
            Class method that checks if the login 
            password hash is the same as the one stored 
            in the database.

        validate_user_name (data: `dict`)
            Class method that does the format validation 
            of the name value provided.

        validate_user_cpf (self, key: `str`, cpf: `str`)
            Class method that does the format validation 
            of the cpf value provided.

        validate_user_email (self, key: `str`, email: `str`)
            Class method that does the format validation 
            of the email value provided.

        validate_user_phone (self, key: `str`, phone: `str`)
            Class method that does the format validation 
            of the phone value provided.

        validate_user_role (self, key: `str`, role: `str`)
            Class method that validates the user role value 
            provided. Can be member or admin.

        validate_user_subscription (self, key: `str`, subscription: `str`)
            Class method that validates the user subscription 
            value provided. Can be free, MEI, small business 
            or enterprise.

        validate_user_password (data: `dict`)
            Class method that does the format validation 
            of the password value provided.

        check_required_keys (data: `dict`)
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.

        input_id_values (data: `dict`)
            Class method that inputs the id values 
            to the request data.
        
        check_key_type (data: `dict`)
            Class method that checks the type of 
            each key value provided.

        check_if_value_is_already_in_use (data: `dict`)
            Class method to check if value is already in use.

        check_user_email (user_email: `str`)
            Class method that checks if the specified user 
            exists in the database.
    """
    
    name: str
    cpf: str
    email: str
    phone: str
    balance: float
    role: str
    subscription: str
    image_url: str
    created_at: datetime

    __tablename__ = 'users'

    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String, nullable=False)
    cpf = db.Column(db.String, nullable=False, unique=True)
    email = db.Column(db.String, nullable=False, unique=True)
    phone = db.Column(db.String, nullable=False, unique=True)
    balance = db.Column(db.Float, default=0)
    role = db.Column(db.String)
    subscription = db.Column(db.String)
    image_url = db.Column(db.String)
    created_at = db.Column(db.DateTime, default=datetime.now())

    password_hash = db.Column(db.String, nullable=True)

    account = relationship("AccountModel", cascade="all, delete", backref="users")
    expense = relationship("ExpenseModel", cascade="all, delete", backref="users")
    hint = relationship("HintModel", cascade="all, delete", backref="users")


    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")


    @password.setter
    def password(self, password_to_hash: str) -> None:
        """
            Class setter method that saves the user's 
            password as a hash in the database.

            Parameters
            ----------
            password_to_hash : `str`
                A string representing the user's password
        """

        """
            Generating the password hash and assigning 
            it to the password_hash column of the 
            database
        """
        self.password_hash = generate_password_hash(password_to_hash)


    def verify_password(self, password_to_compare: str) -> bool:
        """
            Class method that checks if the login 
            password hash is the same as the one stored 
            in the database.

            Parameters
            ----------
            password_to_compare : `str`
                A string representing the password that 
                the user passed at login
        """

        return check_password_hash(self.password_hash, password_to_compare)
    

    @validates('cpf')
    def validate_user_cpf(self, key: str, cpf: str) -> str:
        """
            Class method that does the format validation 
            of the cpf value provided.

            Parameters
            ----------
            key : str
                A string representing the name of the 
                key to be validated (cpf, in this case)

            cpf : str
                A string representing the value of the 
                key to be validated
            
            Raises
            ------
                FormatErrorException
                    If cpf value format does not match 
                    the pattern, it will raises an 
                    exception
        """

        """
            Defining the pattern format of the cpf value 
            that will be accepted
        """
        PATTERN: Literal = "(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)"
        
        """
            Checking if the cpf value format matches the
            defined pattern. If false, it will raise an
            exception
        """
        if not re.fullmatch(PATTERN, cpf):
            raise FormatErrorException("Invalid CPF format. Use the format: xxx.xxx.xxx-xx")

        return cpf


    @validates('email')
    def validate_user_email(self, key: str, email: str) -> str:
        """
            Class method that does the format validation 
            of the email value provided.

            Parameters
            ----------
            key : str
                A string representing the name of the 
                key to be validated (email, in this case)

            email : str
                A string representing the value of the 
                key to be validated
            
            Raises
            ------
                FormatErrorException
                    If email value format does not match 
                    the pattern, it will raises an 
                    exception
        """

        """
            Defining the pattern format of the email 
            value that will be accepted
        """
        PATTERN: Literal = "^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
        
        """
            Checking if the email value format matches 
            the defined pattern. If false, it will raise 
            an exception
        """
        if not re.fullmatch(PATTERN, email):
            raise FormatErrorException("Invalid email format. Use the format: exemplo@email.com")

        return email


    @validates('phone')
    def validate_user_phone(self, key: str, phone: str) -> str:
        """
            Class method that does the format validation 
            of the phone value provided.

            Parameters
            ----------
            key : str
                A string representing the name of the 
                key to be validated (email, in this case)

            phone : str
                A string representing the value of the 
                key to be validated
            
            Raises
            ------
                FormatErrorException
                    If phone value format does not match 
                    the pattern, it will raises an 
                    exception
        """

        """
            Defining the pattern format of the phone 
            value that will be accepted
        """
        PATTERN: Literal = "\(\d{2}\)\d{5}-\d{4}"
        
        """
            Checking if the phone value format matches 
            the defined pattern. If false, it will raise 
            an exception
        """
        if not re.fullmatch(PATTERN, phone):
            raise FormatErrorException("Invalid phone format. Use the format: (xx)xxxxx-xxxx")

        return phone

    
    @validates('role')
    def validate_user_role(self, key: str, role: str) -> str:
        """
            Class method that validates the user role value 
            provided. Can be member or admin.

            Parameters
            ----------
            key : str
                A string representing the name of the 
                key to be validated (role, in this case)

            phone : str
                A string representing the value of the 
                key to be validated
            
            Raises
            ------
                InvalidCategoryException
                    If the role value is not acceptable, 
                    it will raises an exception
        """

        """
            Defining a tuple of role values ​​that will be 
            accepted
        """
        ROLES_TYPES: tuple = ("Member", "Admin")

        """ 
            Checking if the role value provided is in the 
            tuple of acceptable role values. If it is not,
            it will raises an exception
        """
        if role.title() not in ROLES_TYPES:
            raise InvalidRoleException(f"Invalid role, the role must be: {ROLES_TYPES}")

        return role


    @validates('subscription')
    def validate_user_subscription(self, key: str, subscription: str) -> str:
        """
            Class method that validates the user subscription 
            value provided. Can be free, MEI, small business 
            or enterprise.

            Parameters
            ----------
            key : str
                A string representing the name of the 
                key to be validated (subscription, in this 
                case)

            phone : str
                A string representing the value of the 
                key to be validated
            
            Raises
            ------
                InvalidCategoryException
                    If the subscription value is not 
                    acceptable, it will raises an exception
        """

        """
            Defining a tuple of subscription values ​​that 
            will be accepted
        """
        SUBSCRIPTIONS_TYPES = ("Free", "MEI", "Small Business", "Enterprise")

        """ 
            Checking if the subscription value provided is in 
            the tuple of acceptable subscription values. If it 
            is not, it will raises an exception
        """
        if subscription not in SUBSCRIPTIONS_TYPES:
            raise InvalidCategoryException(f"Invalid subscription, the subscription must be: {SUBSCRIPTIONS_TYPES}")

        return subscription


    @staticmethod
    def format_user_name(data: dict) -> dict:
        """
            Class method that formats the username.

            Parameters
            ----------
            data : dict
                The data returned from the request
        """

        """
            Transforms the name, turning the first
            letter of each word into capital letters,
            and others into lower case letters
        """
        data['name'] = data['name'].title()

        return data


    @staticmethod
    def validate_user_password(data: dict) -> str:
        """
            Class method that does the format validation 
            of the password value provided.

            Parameters
            ----------
            data: dict
                The data returned from the request
            
            Raises
            ------
                FormatErrorException
                    If password value format does not match 
                    the pattern, it will raises an 
                    exception
        """

        password: str = data['password']

        """
            Defining the pattern format of the password 
            value that will be accepted
        """
        PATTERN: Literal = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$"
        
        """
            Checking if the password value format matches 
            the defined pattern. If false, it will raise 
            an exception
        """
        if not re.fullmatch(PATTERN, password):
            raise FormatErrorException("Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character")


    @staticmethod
    def check_required_keys(data: dict) -> None:
        """
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            MissingKeyException
                If the data does not contain the
                required keys, it will raises an
                exception

            InvalidKeyException
                If the data contains keys that does
                not existis in the valid keys, it
                will raises an exception
        """

        REQUIRED_KEYS: tuple = ("name", "cpf", "email", "phone", "password",)
        VALID_KEYS: tuple = ("name", "cpf", "email", "phone", "password", "role", "subscription", "image_url", "created_at",)
        missing_keys: list = []
        invalid_keys: list = []

        """
            Checking if the request data contains
            the required keys. If not, the missing
            keys will be appended to a list.
        """
        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        """
            Checking if the missing_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        """
            Checking if the request data contains
            only valid keys. If not, the invalid
            keys will be appended to a list.
        """
        for key in data:
            if key not in VALID_KEYS:
                invalid_keys.append(key)

        """
            Checking if the invalid_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')

        
    @staticmethod
    def input_id_values(data: dict) -> dict:
        """
            Class method that inputs the id values 
            to the request data.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values
        """

        """
            Creating the expense id combining randomly 
            128 digits, upper and lower letters.
        """
        id: str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(128))

        data['id'] = id

        return data 


    @staticmethod
    def check_key_type(data: dict) -> None:
        """
            Class method that checks the type of 
            each key value provided.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            InvalidFieldType
                If the provided keys has values that
                not are instances of the correct types,
                it will raises an exception
        """
        
        STRING_KEYS: tuple = ("name", "cpf", "email", "phone", "password", "role", "subscription", "image_url", "created_at",)

        """
            Checking if the request data contains the key 
            of the tuple. If true, it will raise an exception
        """
        for key in STRING_KEYS:
            if key in data:
                if not isinstance(data[key], str):
                    raise InvalidFieldType(f"The key '{key}' are not instance of type string.")


    @staticmethod
    def check_if_value_is_already_in_use(data: dict) -> None:
        """
            Class method that checks if value is already 
            in use.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
                ValueAlreadyInUseException
                    If the value already in use, it will 
                    raises an exception
        """

        cpf: list = UserModel.query.filter_by(cpf = data["cpf"]).first()
        phone: list = UserModel.query.filter_by(phone = data["phone"]).first()
        email: list = UserModel.query.filter_by(email = data["email"]).first()

        """
            Checking if cpf, phone or email already exists in
            the database. If true, it will raise an exception
        """
        if cpf:
            raise ValueAlreadyInUseException("CPF Already in use")
        if phone:
            raise ValueAlreadyInUseException("Phone Already in use")
        if email:
            raise ValueAlreadyInUseException("Email Already in use")


    @staticmethod
    def check_user_email (user_email: str) -> None:
        """
            Class method that checks if the specified user 
            exists in the database.

            Parameters
            ----------
            user_email : `str`
                A string representing the user's email provided

            Raises
            ------
            NotFoundUserError
                If the email does not exist in the database,
                it will raises an exception
        """

        user: list = UserModel.query.filter_by(email = user_email).first()

        """
            Checking if there is no user in the database with 
            the email provided. If true, it will raise an 
            exception
        """
        if user is None:
            raise NotFoundUserError('User not found!')

    
    @staticmethod
    def check_update_keys(data: dict) -> None:

        REQUIRED_KEYS: tuple = ('email',)
        VALID_KEYS: tuple = ('email', 'name', 'cpf', 'phone', 'role', 'subscription')
        missing_keys: list = []
        invalid_keys: list = []
        total_of_keys: int = 0

        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        for key in data:
            if key not in VALID_KEYS:
                invalid_keys.append(key)
            else:
                total_of_keys += 1

        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')   

        if total_of_keys < 2:
            raise InvalidTotalOfKeysException(f'You need to send the email and another valid key.')  


    @staticmethod
    def check_delete_keys(data: dict) -> None:

        REQUIRED_KEYS: tuple = ('email',)
        missing_keys: list = []
        invalid_keys: list = []

        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        for key in data:
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')

    
    def get_reset_token(self, expires=500):
        """
            Sending a email to a user with a link
            to change his password
        """
        return jwt.encode({'reset_password': self.email, 'exp': time() + expires},
                           key=os.getenv('SECRET_KEY_FLASK'))


    @staticmethod
    def verify_reset_token(token):
        try:
            user_email = jwt.decode(token, key=os.getenv('SECRET_KEY_FLASK'))['reset_password']
        except Exception as e:
            print(e)
            return
        return UserModel.query.filter_by(email=user_email).first()

    
    @staticmethod
    def verify_email(user_email: str) -> None:
        """
            Class method that checks if the specified user 
            exists in the database.

            Parameters
            ----------
            user_email : `str`
                A string representing the user's email provided
        """

        user = UserModel.query.filter_by(email=user_email).first()

        return user


    def set_password(self, password, commit=False):
        self.password_hash = generate_password_hash(password)
