from app.configs.database import db
from dataclasses import dataclass
import random, string
from app.exceptions.public_exceptions import (
    InvalidFieldType,
    MissingKeyException,
    InvalidKeyException,
    InvalidTypeException,
    InvalidValueException,
)


@dataclass
class LoanModel(db.Model):
    """
        Class that represents a loan simulation

        ...

        Attributes
        ----------
        payments : `str`
            payment amounts per period

        amortizations : `str`
            amortization amounts per period

        interests : `str`
            interes amounts per period

        balances : `str`
            debit balance amounts per period

        Methods
        -------
        check_required_keys (data: `dict`)
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.
        
        check_key_type (data: `dict`)
            Class method that checks the type of each 
            key value provided

        check_values (data: `dict`)
            Class method that checks if
            each key value provided is valid        

        input_id_values (data: `dict`)
            Class method that inserts id values 
            ​​into the data that will be passed to 
            create a loan object
    """

    payments: list
    amortizations: list
    interests: list
    balances: list

    __tablename__ = 'loans'

    id = db.Column(db.String, primary_key=True)
    payments = db.Column(db.ARRAY(db.String))
    amortizations = db.Column(db.ARRAY(db.String))
    interests = db.Column(db.ARRAY(db.String))
    balances = db.Column(db.ARRAY(db.String), nullable=False)    


    @staticmethod
    def check_required_keys(data: dict) -> None:
        """
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            MissingKeyException
                If the data does not contain the
                required keys, it will raises an
                exception

            InvalidKeyException
                If the data contains keys that does
                not existis in the valid keys, it
                will raises an exception
        """

        REQUIRED_KEYS: tuple = ("loan_amount", "loan_term", "loan_type", "interest_rate")
        VALID_KEYS: tuple = ("loan_amount", "loan_term", "loan_type", "interest_rate")
        missing_keys: list = []
        invalid_keys: list = []

        """
            Checking if the request data contains
            the required keys. If not, the missing
            keys will be appended to a list.
        """
        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        """
            Checking if the missing_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        """
            Checking if the request data contains
            only valid keys. If not, the invalid
            keys will be appended to a list.
        """
        for key in data:
            if key not in VALID_KEYS:
                invalid_keys.append(key)

        """
            Checking if the invalid_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')


    @staticmethod
    def check_key_type(data: dict) -> None:
        """
            Class method that checks the type of 
            each key value provided 

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            InvalidFieldType
                If the provided keys has values that
                not are instances of the correct types,
                it will raises an exception
        """

        STRING_KEYS: tuple = ("loan_type",)
        FLOAT_KEYS: tuple = ("loan_amount", "interest_rate")
        INTEGER_KEYS: tuple = ("loan_term",)

        """
            First, checking if the request data
            contains the key of the list.
            Second, checking if the value assimiled
            to the key, is not instance of type 
            string. If true, it will raise an exception.
        """
        for key in STRING_KEYS:
            if key in data:
                if not isinstance(data[key], str):
                    raise InvalidFieldType(f"The key '{key}' are not instance of type string.")

        """
            Same as before, but in this time we will
            check if the value is not instance of
            type float.
        """
        for key in FLOAT_KEYS:
            if key in data:
                if not isinstance(data[key], float):
                    raise InvalidFieldType(f"The key '{key}' are not instance of type float.")

        """
            Same as before, but in this time we will
            check if the value is not instance of
            type integer.
        """
        for key in INTEGER_KEYS:
            if key in data:
                if not isinstance(data[key], int):
                    raise InvalidFieldType(f"The key '{key}' are not instance of type integer.")

    
    @staticmethod
    def check_values(data: dict) -> None:
        """
            Class method that checks if
            each key value provided is valid

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            InvalidTypeException
                If the type key does not have a valid
                value, it will raises an exception

            InvalidCategoryException
                If the category key does not have a
                valid value, it will raises an 
                exception

            InvalidValueException
                If one of the numeric keys contains
                a negative value, it will raises an
                exception
        """

        LOAN_TYPE_VALID_VALUES: tuple = ("price", "sac")
        numeric_keys: list = ["loan_amount", "loan_term", "interest_rate"]

        """
            Checking if the value assimiled to the
            type key is a valid value. If false, it
            will raise an error.
        """
        if data["loan_type"].lower() not in LOAN_TYPE_VALID_VALUES:
            raise InvalidTypeException('The loan_type must be price or sac.')

        """
            Checking if the numeric keys contains
            only positive numbers. If false, it will
            raise an error.
        """
        for key in numeric_keys:
            if data[key] < 0:
                raise InvalidValueException('The loan_amount, loan_term and interest_rate must be positive numbers.')


    @staticmethod
    def input_id_values(data: dict) -> dict:
        """
            Class method that inserts id values 
            ​​into the data that will be passed to 
            create a loan object

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values
        """

        """
            Creating the expense id combining randomly 
            128 digits, upper and lower letters.
        """
        id: str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(128))

        data['id'] = id

        return data 
        