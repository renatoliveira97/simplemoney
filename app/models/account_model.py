from app.configs.database import db
import string, random
from dataclasses import dataclass
from flask_jwt_extended import get_jwt_identity
from app.exceptions.public_exceptions import (
    MissingKeyException,
    InvalidKeyException,
    InvalidFieldType,
    InvalidValueException
)
from app.exceptions.account_exceptions import (
    AccountAlreadyExists
)
from app.models.user_model import UserModel


@dataclass
class AccountModel(db.Model):
    """
    Class that represents the account for
    balance administration

    ...
    
    Attributes
    ----------
    name : `str` 
        The name of the expense and indentificator

    balance : `float`
            the user's account balance
    
    user_email : `str`
            the user's email

    Methods
    -------
    check_keys(data: `dict`)
        Class method that checks if the request
        data contains all the required keys, and
        only contains valid key

    check_values (data: `dict`)
            Class method that checks if
            each key value provided is valid

    check_if_name_already_exists(category_name: `str`)
        Class method that checks if the name
        exists inside the database.

    input_values (data: `dict`)
            Class method that inputs the values 
            to the request data.
    
    input_id (data: `dict`)
            Class method that inputs the id 
            to the request data.

    check_delete_keys(data: `dict`)
        Class method that checks if the request
        data contains all the required keys to delete a user.

    """

    id: str
    name: str
    balance: float
    user_email: str

    __tablename__ = 'accounts'

    id = db.Column(db.String, primary_key = True)
    name = db.Column(db.String, nullable = False)
    balance = db.Column(db.Float, default = 0)
    user_email = db.Column(db.String, db.ForeignKey("users.email"))


    @staticmethod
    def check_keys(data: dict) -> None:
        """
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            MissingKeyException
                If the data does not contain the
                required keys, it will raises an
                exception

            InvalidKeyException
                If the data contains keys that does
                not existis in the valid keys, it
                will raises an exception
        """

        REQUIRED_KEYS: tuple = ('name',)
        missing_keys: list = []
        invalid_keys: list = []

        """
            Checking if the request data contains
            the required keys. If not, the missing
            keys will be appended to a list.
        """
        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        """
            Checking if the missing_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        """
            Checking if the request data contains
            only required keys. If not, the invalid
            keys will be appended to a list.
        """
        for key in data:
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        """
            Checking if the invalid_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')


    @staticmethod
    def check_values(data: dict) -> None:
        """
            Class method that checks if
            values provided is valid

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            InvalidFieldType
                If the field does not have a valid
                type value, it will raises an exception

        """
        for value in data.values():
            if not isinstance(value, str):
                raise InvalidFieldType(f'The value {value} is not instance of type String.')


    @staticmethod
    def check_if_name_already_exists(data: dict) -> None:
        """
            Class method that checks if name is already 
            in use.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
                AccountAlreadyExists
                    If the name already in use, it will 
                    raises an exception
        """
        current_user: dict = get_jwt_identity()

        user_email: str = current_user['email']

        accounts: list = AccountModel.query.filter_by(user_email = user_email)

        account: list = []

        for acc in accounts:
            if acc['name'] == data['name']:
                account.append(acc)

        if account != []:
            raise AccountAlreadyExists(f'This account already exists, please select a new name.')

    
    @staticmethod
    def input_values(data: dict) -> dict:
        """
            Class method that inserts values 
            ​​into the data that will be passed to 
            create an account

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values
        """

        """
            Creating the account combining randomly 
            128 digits, upper and lower letters.
        """

        id: str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(128))

        current_user: dict = get_jwt_identity()

        user_email: str = current_user['email']

        data['id'] = id
        data['user_email'] = user_email

        return data


    @staticmethod
    def input_id(data: dict) -> dict:
        """
            Class method that inserts id 
            ​​into the data that will be passed to 
            create an account

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values
        """

        """
            Creating the account combining randomly 
            128 digits, upper and lower letters.
        """
        id: str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(128))

        data['id'] = id

        return data


    @staticmethod
    def check_delete_key(data: dict) -> None:

        """
            Class method that checks if the request
            data contains all the required keys to
            delete an account.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            MissingKeyException
                If the data does not contain the
                required keys, it will raises an
                exception

            InvalidKeyException
                If the data contains keys that does
                not existis in the valid keys, it
                will raises an exception
        """

        REQUIRED_KEY: tuple = ('id',)
        missing_keys: list = []
        invalid_keys: list = []

        """
            Checking if the request data contains
            the required keys. If not, the missing
            keys will be appended to a list.
        """
        for key in REQUIRED_KEY:
            if key not in data:
                missing_keys.append(key)

        """
            Checking if the missing_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        """
            Checking if the required keys list
            contains any value. If true, it will
            raise an exception.
        """
        for key in data:
            if key not in REQUIRED_KEY:
                raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')
