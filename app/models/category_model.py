from app.configs.database import db
from dataclasses import dataclass

from app.exceptions.category_exceptions import (
    CategoryNotFoundException
)
from app.exceptions.public_exceptions import (
    InvalidKeyException, 
    MissingKeyException
)


@dataclass
class CategoryModel(db.Model):
    """
    Class that represents the category of a expense
    for persoanl finance administration

    ...
    
    Attributes
    ----------
    name : `str` 
        The name of the expense and indentificator

    description : `str` 
        The full description of the category 

    Methods
    -------
    check_category_exist(category_name: `str`)
        Class method that checks if the category name
        exists inside the database, if it exists,
        assimilate it to the 'category' variable.

    check_required_keys(data: `dict`)
        Class method that checks if the request
        data contains all the required keys, and
        only contains valid key
    
    check_description_key(data: `dict`)
        Class method that checks if the key
        provided by the request is
        the 'description'.
    """
    
    name: str
    description: str

    __tablename__ = "categories"

    name = db.Column(db.String, primary_key=True)
    description = db.Column(db.String, nullable=False)


    @staticmethod
    def check_category_exist(category_name: str) -> None:
        """
            Class method that checks if the category exists
            inside the database.

            Parameters
            ----------
            category_name : `str`
                A string with the name of
                the category to be checked 

            Raises
            ------
            CategoryNotFoundException
                If the category does not
                exists in database, it
                will raises an exception
        """


        """
            Checking if the category name
            exists inside the database, if it exists,
            assimilate it to the 'category' variable,
            if not, raise an error.
        """
        category: CategoryModel = CategoryModel.query.get(category_name)
        
        if not category:
            raise CategoryNotFoundException("Category not found!")

    
    @staticmethod
    def check_required_keys(data: dict) -> None:
        """
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            MissingKeyException
                If the data does not contain the
                required keys, it will raises an
                exception

            InvalidKeyException
                If the data contains keys that does
                not existis in the valid keys, it
                will raises an exception
        """

        REQUIRED_KEYS: tuple = ("name", "description")
        missing_keys: list = []
        invalid_keys: list = []

        """
            Checking if the request data contains
            the required keys. If not, the missing
            keys will be appended to a list.
        """
        for key in REQUIRED_KEYS:
            if key not in data.keys():
                missing_keys.append(key)

        """
            Checking if the missing_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if missing_keys != []:
            raise MissingKeyException(f"The keys {missing_keys} are missing!")

        """
            Checking if the request data contains
            only valid keys. If not, the invalid
            keys will be appended to a list.
        """
        for key in data.keys():
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        """
            Checking if the invalid_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if invalid_keys != []:
            raise InvalidKeyException(f"The keys {invalid_keys} are invalid!")
