from builtins import staticmethod
from app.configs.database import db
from dataclasses import dataclass

from app.exceptions.portfolio_exceptions import (
    InvalidKeyException,
    MissingKeyException,
    PortfolioDoesNotExist
)


@dataclass
class PortfolioModel(db.Model):
    name: str
    profile_type: str
    stocks: str
    objective: str

    __tablename__ = 'portfolios'

    name = db.Column(db.String, primary_key=True)
    profile_type = db.Column(db.String, nullable=False)
    stocks = db.Column(db.String)
    objective = db.Column(db.String, nullable=False)


    @staticmethod
    def check_required_keys(data: dict) -> None:
        """
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            MissingKeyException
                If the data does not contain the
                required keys, it will raises an
                exception

            InvalidKeyException
                If the data contains keys that does
                not existis in the valid keys, it
                will raises an exception
        """

        REQUIRED_KEYS: tuple = ("name", "profile_type", "objective")
        VALID_KEYS: tuple = ("name", "profile_type", "stocks", "objective")
        missing_keys: list = []
        invalid_keys: list = []

        """
            Checking if the request data contains
            the required keys. If not, the missing
            keys will be appended to a list.
        """
        for key in REQUIRED_KEYS:
            if key not in data.keys():
                missing_keys.append(key)

        """
            Checking if the missing_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if missing_keys != []:
            raise MissingKeyException(f"The keys {missing_keys} are missing!")

        """
            Checking if the request data contains
            only valid keys. If not, the invalid
            keys will be appended to a list.
        """
        for key in data.keys():
            if key not in VALID_KEYS:
                invalid_keys.append(key)

        """
            Checking if the invalid_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if invalid_keys != []:
            raise InvalidKeyException(f"The keys {invalid_keys} are invalid!")


    @staticmethod
    def check_keys_to_update(data: dict):

        VALID_KEYS: tuple = ("profile_type", "stocks", "objective", )
        invalid_keys = []

        for key in data.keys():
            if key not in VALID_KEYS:
                invalid_keys.append(key)

        if invalid_keys != []:
            raise InvalidKeyException(f"The keys {invalid_keys} are invalid!")


    @staticmethod
    def check_portfolio_exist(name: str):

        portfolio = PortfolioModel.query.get(name)

        if not portfolio:
            raise PortfolioDoesNotExist("Portfolio doesn't exist!")
