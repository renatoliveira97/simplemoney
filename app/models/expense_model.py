from dataclasses import dataclass
from sqlalchemy.orm import relationship
from app.configs.database import db
from flask_jwt_extended import get_jwt_identity
import random, string
from app.models.user_model import UserModel
from app.exceptions.expense_exceptions import (
    InvalidCategoryException
)
from app.exceptions.public_exceptions import (
    InvalidFieldType,
    MissingKeyException,
    InvalidKeyException,
    InvalidTypeException,
    InvalidValueException
)


@dataclass
class ExpenseModel(db.Model):
    """
    Class that represents an expense model for
    personal finance administration

    ...

    Attributes
    ----------
    id : `str` 
        the expense identificator

    name : `str` 
        the name of the expense

    value : `float` 
        the value of the expense. Can be a cost 
        or an income

    quota : `int`, optional 
        this value represents the total quotas that 
        the expense is divided

    actual_quota : `int` 
        the actual part of the payment

    type : `str` 
        the type of the expense. Must be a cost or 
        an income

    category : `str` 
        the category of the expense

    user_email : `str` 
        the email of the relationed user

    account_id : `str`
        the identification of the relationed account

    Methods
    -------
    check_required_keys (data: `dict`)
        Class method that checks if the request
        data contains all the required keys, and
        only contains valid keys.
       
    input_values (data: `dict`)
        Class method that inputs the id and the
        user_email values to the request data.

    check_key_type (data: `dict`)
        Class method that checks the type of each 
        key value provided

    check_values (data: `dict`)
        Class method that checks  if each key 
        value provided is valid
    """

    id: str
    name: str
    value: float
    quota: int
    actual_quota: int
    type: str
    category: str
    user_email: str
    account_id : str

    __tablename__ = "expenses"

    id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String, nullable=False)
    value = db.Column(db.Float, nullable=False)
    quota = db.Column(db.Integer, default=1)
    actual_quota = db.Column(db.Integer)
    type = db.Column(db.String, nullable=False)
    category = db.Column(db.String, db.ForeignKey("categories.name"))
    user_email = db.Column(db.String, db.ForeignKey("users.email"))
    account_id = db.Column(db.String, db.ForeignKey("accounts.id"))

    rel_category = relationship('CategoryModel', backref='expenses', uselist=False)
    rel_account = relationship('AccountModel', backref='expenses', uselist=False)
    

    @staticmethod
    def check_required_keys(data: dict) -> None:
        """
            Class method that checks if the request
            data contains all the required keys, and
            only contains valid keys.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            MissingKeyException
                If the data does not contain the
                required keys, it will raises an
                exception

            InvalidKeyException
                If the data contains keys that does
                not existis in the valid keys, it
                will raises an exception
        """

        REQUIRED_KEYS: tuple = ("name", "value", "type", "category", "account_id")
        VALID_KEYS: tuple = ("name", "value", "quota", "actual_quota", "type", "category", "account_id")
        missing_keys: list = []
        invalid_keys: list = []

        """
            Checking if the request data contains
            the required keys. If not, the missing
            keys will be appended to a list.
        """
        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        """
            Checking if the missing_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        """
            Checking if the request data contains
            only valid keys. If not, the invalid
            keys will be appended to a list.
        """
        for key in data:
            if key not in VALID_KEYS:
                invalid_keys.append(key)

        """
            Checking if the invalid_keys list
            contains any value. If true, it will
            raise an exception.
        """
        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')


    @staticmethod
    def input_values(data: dict) -> dict:
        """
            Class method that inputs the id and the
            user_email values to the request data.

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values
        """

        """
            Creating the expense id combining randomly 
            128 digits, upper and lower letters.
        """
        id: str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(128))
        
        """
            Getting the logged user information
            and assimilating to a variable.
        """
        current_user: dict = get_jwt_identity()

        """
            Separating the email value from the
            current_user to assimilate to the
            request data.
        """
        user_email: str = current_user['email']

        data['id'] = id
        data['user_email'] = user_email

        return data

    
    @staticmethod
    def check_key_type(data: dict) -> None:
        """
            Class method that checks the type of 
            each key value provided 

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            InvalidFieldType
                If the provided keys has values that
                not are instances of the correct types,
                it will raises an exception
        """

        STRING_KEYS: tuple = ("name", "type", "category", "account_id")
        FLOAT_KEYS: tuple = ("value",)
        INTEGER_KEYS: tuple = ("quota", "actual_quota")

        """
            First, checking if the request data
            contains the key of the list.
            Second, checking if the value assimiled
            to the key, is not instance of type 
            string. If true, it will raise an exception.
        """
        for key in STRING_KEYS:
            if key in data:
                if not isinstance(data[key], str):
                    raise InvalidFieldType(f"The key '{key}' are not instance of type string.")

        """
            Same as before, but in this time we will
            check if the value is not instance of
            type float.
        """
        for key in FLOAT_KEYS:
            if key in data:
                if not isinstance(data[key], float):
                    raise InvalidFieldType(f"The key '{key}' are not instance of type float.")

        """
            Same as before, but in this time we will
            check if the value is not instance of
            type integer.
        """
        for key in INTEGER_KEYS:
            if key in data:
                if not isinstance(data[key], int):
                    raise InvalidFieldType(f"The key '{key}' are not instance of type integer.")


    @staticmethod
    def get_value(data: dict) -> int:
        """
            Class method that check the type of the
            expense and get the value 

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values
        """

        value_type: str = data['type']

        value = float(data['value'])

        """
            Testing if the expense type is a cost
            or an income
        """
        if value_type == 'income':
            return value
        else:
            return -value


    @staticmethod
    def check_values(data: dict) -> None:
        """
            Class method that checks if
            each key value provided is valid

            Parameters
            ----------
            data : `dict`
                A dictionary with the response keys
                and values

            Raises
            ------
            InvalidTypeException
                If the type key does not have a valid
                value, it will raises an exception

            InvalidCategoryException
                If the category key does not have a
                valid value, it will raises an 
                exception

            InvalidValueException
                If one of the numeric keys contains
                a negative value, it will raises an
                exception
        """

        TYPE_VALID_VALUES: tuple = ("cost", "income")
        CATEGORY_VALID_VALUES: tuple = (
            "alimentation", "health", "clothes", "travel", 
            "infrastructure", "junk food", "locomotion",
            "recreation", "sports", "culture", "studies",
            "investments", "stocks", "job", "equity",
            "taxes", "fees", "public titles", "private titles",
            "dividends", "properties", "communication",
            "social", "donation", "work", "personal goods"
        )
        QUOTA_KEYS: tuple = ("quota", "actual_quota")
        numeric_keys: list = ["value"]

        """
            Checking if the request data contains
            the quota keys. If true, it will append
            the existing keys to a list.
        """
        for key in QUOTA_KEYS:
            if key in data:
                numeric_keys.append(key)

        """
            Checking if the value assimiled to the
            type key is a valid value. If false, it
            will raise an error.
        """
        if data["type"] not in TYPE_VALID_VALUES:
            raise InvalidTypeException('The type must be a cost or an income.')

        """
            Checking if the value assimiled to the
            category key is a valid value. If false,
            it will raise an error.
        """
        if data["category"] not in CATEGORY_VALID_VALUES:
            raise InvalidCategoryException(f'The category {data["category"]} is invalid.')

        """
            Checking if the numeric keys contains
            only positive numbers. If false, it will
            raise an error.
        """
        for key in numeric_keys:
            if data[key] < 0:
                raise InvalidValueException('The value, quota and actual_quota must be positive numbers.')
