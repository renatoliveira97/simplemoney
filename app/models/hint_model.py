from app.configs.database import db
from sqlalchemy.orm import relationship
import random, string
from flask_jwt_extended import get_jwt_identity
from dataclasses import dataclass
from app.static.dictionaries.hints import hints_list
from app.exceptions.public_exceptions import (
    MissingKeyException,
    InvalidKeyException,
    InvalidValueException
)
from app.exceptions.hint_exception import (
    HintNotFoundException
)
from app.models.user_model import UserModel


@dataclass
class HintModel(db.Model):
    """
    Class that represents the strategic financial
    management hints.

    Attributes
    ----------
    id : `str`
        the hint identificator

    title : `str`
        the hint title

    body : `str`
        the body of the hint

    references : `str`
        the bibliographic references for the hint

    type : `str`
        the type of the hint. Must be investment,
        income, entrepreneurship, loan, financing, 
        cost, tax or fee management

    user_email : `str` 
        the email of the relationed user
    """

    id: str
    title: str
    body: str
    references: str
    type: str
    user_email: str

    __tablename__ = "hints"

    id = db.Column(db.String, primary_key = True)
    title = db.Column(db.String, nullable = False)
    body = db.Column(db.String, nullable = False)
    references = db.Column(db.String)
    type = db.Column(db.String, nullable = False)
    user_email = db.Column(db.String, db.ForeignKey("users.email"))


    @staticmethod
    def check_default_keys(data: dict) -> None:

        REQUIRED_KEYS: tuple = ("type", "title")
        missing_keys: list = []
        invalid_keys: list = []

        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        for key in data:
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')
        

    @staticmethod
    def check_default_values(data: dict) -> None:

        DEFAULT_TYPE_VALUES: tuple = ('Personal Finance', 'Investments')
        DEFAULT_TITLE_VALUES: tuple = ('PFP: Consumer', 'PFP: Enthusiastic', 'PFP: Investor', 
            'PFP: Entrepreneur', 'IP: Conservative', 'IP: Moderate', 'IP: Risk Prone')
        
        hint_type: str = data['type']
        hint_title: str = data['title']

        if hint_type not in DEFAULT_TYPE_VALUES:
            raise InvalidValueException(f'The type {hint_type} is invalid, please select {DEFAULT_TYPE_VALUES}.')

        if hint_title not in DEFAULT_TITLE_VALUES:
            raise InvalidValueException(f'The type {hint_title} is invalid, please select {DEFAULT_TITLE_VALUES}.')


    @staticmethod
    def input_default_values(data: dict, title: str) -> dict:

        id: str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(128))

        current_user: UserModel = get_jwt_identity()

        user_email: str = current_user['email']

        for hint in hints_list:
            if hint['title'] == title:
                body: str = hint['body']

        data['id'] = id
        data['body'] = body
        data['user_email'] = user_email

        return data


    @staticmethod
    def check_custom_keys(data: dict) -> None:

        REQUIRED_KEYS: tuple = ('title', 'body', 'type')
        missing_keys: list = []
        invalid_keys: list = []

        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        for key in data:
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')


    @staticmethod
    def check_custom_values(data: dict) -> None:

        HintModel.check_string_values(data)


    @staticmethod
    def input_id_value(data: dict) -> dict:

        id: str = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(128))

        data['id'] = id

        return data

    
    @staticmethod
    def check_update_keys(data: dict) -> None:

        REQUIRED_KEYS: tuple = ('body', 'type', 'user_email')
        missing_keys: list = []
        invalid_keys: list = []

        for key in REQUIRED_KEYS:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')

        for key in data:
            if key not in REQUIRED_KEYS:
                invalid_keys.append(key)

        if invalid_keys != []:
            raise InvalidKeyException(f'The keys {invalid_keys} are invalid.')



    @staticmethod
    def check_update_values(data: dict) -> None:

        HintModel.check_string_values(data)


    @staticmethod
    def get_hint_to_update(data: dict) -> dict:

        user_email: str = data['user_email']
        hint_type: str = data['type']

        hints: list = HintModel.query.filter_by(user_email = user_email).all()

        hint_to_update: dict = {}

        for hint in hints:
            if hint.type == hint_type:
                hint_to_update = hint

        if hint_to_update == {}:
            raise HintNotFoundException(f'This user does not have this type of hint.')

        return hint_to_update


    @staticmethod
    def check_string_values(data: dict) -> None:

        invalid_values: list = []

        for value in data.values():
            if not isinstance(value, str):
                invalid_values.append(value)

        if invalid_values != []:
            raise InvalidValueException(f'The values {invalid_values} are not instance of type string.')
        