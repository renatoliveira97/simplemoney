from flask_jwt_extended import JWTManager
from flask import Flask

def init_app(app: Flask):
    """
        JWT Management Pack Initialization 

    Parameters
    ----------
    app : `Flask` 
        A flask type application to be initialized with JWT manager
    """
    JWTManager(app)
    