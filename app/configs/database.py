from flask import Flask
from flask_sqlalchemy import SQLAlchemy

"""
    Creating a Database Instance with SQLAlchemy 
"""
db = SQLAlchemy()


def init_app(app: Flask):
    """
        Method to create a database instance in the flask application 

    Parameters
    ----------
        app : `Flask` 
            A flask type application to be initialized and configured as database
    """
    
    db.init_app(app)
    app.db = db
    