from flask import render_template
from flask_mail import Message, Mail
import os
from app.models.user_model import UserModel

"""
    Creating an instance of the Mail class for automatic email sending 
"""
mail = Mail()


def send_email(user: UserModel):
    """
        Methods responsible for configuring the email
        that will be automatically sent to recover the password

        Parameters
        ----------
        user: `UserModel`
            An instance of the user class 
    """

    """
        Getting the token responsible for the recovery 
    """
    token = user.get_reset_token()

    """
        Setting all the necessary parameters to send the email 
    """
    msg = Message()
    msg.subject = "Flask App Password Reset"
    msg.sender = os.getenv('MAIL_USERNAME')
    msg.recipients = [user.email]
    msg.html = render_template('reset_email.html', user=user, token=token)
    msg.body = "body"

    mail.send(msg)
