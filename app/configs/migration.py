from flask import Flask
from flask_migrate import Migrate


def init_app(app: Flask):
    """
        Method for creating tables in the database.
        
        Warning
        -------
        Tables must be imported into the function for their creation 

        Parameters
        ----------
        app : `Flask` 
            A flask type application
    """
    from app.models.category_model import CategoryModel
    from app.models.expense_model import ExpenseModel
    from app.models.hint_model import HintModel
    from app.models.user_model import UserModel
    from app.models.loan_model import LoanModel

    Migrate(app, app.db)
    