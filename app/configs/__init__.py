from flask import Flask
import os
from dotenv import load_dotenv

load_dotenv()

def init_app(app: Flask):
    """
        Configuration package launcher where all
        necessary configuration options for
        the application are created 

        Parameters
        ----------
        app : `Flask` 
            A flask type application to be initialized and configured 
    """
    
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('SQLALCHEMY_DATABASE_URI')
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = bool(os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS'))
    app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY')
    app.config["MAIL_SERVER"] = os.environ.get('MAIL_SERVER')
    app.config["MAIL_PORT"] = os.environ.get('MAIL_PORT')
    app.config["MAIL_USE_SSL"] = os.environ.get('MAIL_USE_SSL')
    app.config["SQLALCHEMY_COMMIT_ON_TEARDOWN"] = os.environ.get('SQLALCHEMY_COMMIT_ON_TEARDOWN')
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS')
    app.config["MAIL_USERNAME"] = os.environ.get('MAIL_USERNAME')
    app.config["MAIL_PASSWORD"] = os.environ.get('MAIL_PASSWORD')
    app.config ['UPLOAD_FOLDER'] = os.environ.get('UPLOAD_FOLDER')