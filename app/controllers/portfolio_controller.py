from flask import request, jsonify, current_app
from flask_jwt import jwt_required

from app.exceptions.portfolio_exceptions import InvalidKeyException, MissingKeyException, PortfolioAlreadyExist, PortfolioDoesNotExist
from app.models.portfolio_model import PortfolioModel
from http import HTTPStatus
from . import default_exception_return

from psycopg2 import errors
from sqlalchemy.exc import IntegrityError


def create_portfolio() -> None:
    """
        Method for creating a new stock portfolio 
    """

    try:

        """
            Assimilate the SQLAlchemy session to a
            variable.
        """
        session = current_app.db.session

        """
            Getting the data provided by the
            request and assimilating it to
            the data variable.
        """
        data: dict = request.get_json()

        """
            Checking if the data provided by
            the request is valid and correct.
        """
        PortfolioModel.check_required_keys(data)

        new_portfolio: PortfolioModel = PortfolioModel(**data)

        """
            Adding the portfolio to the
            database and commiting it.
        """
        session.add(new_portfolio)
        session.commit()

        """
            Returning the portfolio in a json
            format with the HTTP status code.
        """
        return jsonify(new_portfolio), HTTPStatus.CREATED

    except MissingKeyException as e:
        return default_exception_return(e)
    except InvalidKeyException as e:
        return default_exception_return(e)
    except PortfolioAlreadyExist as e:
        return default_exception_return(e)
    except IntegrityError:
        if errors.UniqueViolation:
            return {"Error": "Portfolio already exist!"}, HTTPStatus.CONFLICT


def update_portfolio(name: str):
    """
        Method to update portfolio

    Parameters
    ----------
    name: `str`
        A string with the name of the portifolia to be updated 
    """
    try:

        """
            Assimilate the SQLAlchemy session to a
            variable.
        """
        session = current_app.db.session

        """
            Getting the data provided
            by the request and assimilating
            it to the 'data' variable.
        """
        data: dict = request.get_json()

        PortfolioModel.check_portfolio_exist(name)
        PortfolioModel.check_keys_to_update(data)

        portfolio: PortfolioModel = PortfolioModel.query.filter_by(name = name).update(data)

        session.commit()

        portfolio: PortfolioModel = PortfolioModel.query.get(name)

        return jsonify(portfolio), HTTPStatus.OK

    except PortfolioDoesNotExist as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)


def delete_portfolio(name: str):
    """
        Method to delete portfolio

    Parameters
    ----------
    name: `str`
        A string with the name of the portifolia to be deleted 
    """

    try:

        """
            Assimilate the SQLAlchemy session to a
            variable.
        """
        session = current_app.db.session

        """
            Checking if portfolio does exist.
        """
        PortfolioModel.check_portfolio_exist(name)

        """
            Getting the portfolio by his name
            and assimilating it to the
            'portfolio_to_delete' variable.
        """
        portfolio_to_delete: PortfolioModel = PortfolioModel.query.get(name)

        """
            Deleting the portfolio from the
            database and commiting it.
        """
        session.delete(portfolio_to_delete)
        session.commit()

        """
            Return without any content
            and HTTP status code.
        """
        return {}, HTTPStatus.NO_CONTENT

    except PortfolioDoesNotExist as e:
        return default_exception_return(e)


def get_portfolio(name: str):
    """
        Method to obtain a specific portfolio 

    Parameters
    ----------
    name: `str`
        A string with the name of the portifolia to be getted 
    """
    try:

        """
            Checking if portfolio does exist.
        """
        PortfolioModel.check_portfolio_exist(name)

        """
            Getting the portfolio from
            the database and inserting
            inside the portfolio variable.
        """
        portfolio: PortfolioModel = PortfolioModel.query.get(name)

        """
            Returning the portfolio.
        """
        return jsonify(portfolio), HTTPStatus.OK

    except PortfolioDoesNotExist as e:
        return default_exception_return(e)