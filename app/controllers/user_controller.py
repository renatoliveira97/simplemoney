from flask import jsonify, request, current_app, render_template
from http import HTTPStatus
from werkzeug.utils import secure_filename
from app.exceptions.expense_exceptions import InvalidCategoryException
from datetime import timedelta
from app.models.user_model import UserModel
from app.models.account_model import AccountModel
from app.configs.email import send_email
from flask_jwt_extended import create_access_token
from . import default_exception_return
from json import loads
from app.exceptions.expense_exceptions import (
    InvalidCategoryException
)
from app.exceptions.user_exceptions import (
    InvalidRoleException
)
from app.exceptions.public_exceptions import (
    FormatErrorException,
    InvalidFieldType,
    MissingKeyException,
    InvalidKeyException,
    ValueAlreadyInUseException,
    InvalidTotalOfKeysException
)


def create_user():
    """
        Method that creates an user.
        This function will manipulate and validate the data 
        provided by the request, to ensure the object 
        integrity. 
    """
    
    try:    

        session = current_app.db.session

        """
        Get the data provided by the post requisition
        and assimilate to a dictionary.
        """
        data: dict = request.get_json()

        """
            Use the UserModel class methods to check
            the attributes given by the request.
        """
        UserModel.check_key_type(data)
        UserModel.check_required_keys(data)
        UserModel.check_if_value_is_already_in_use(data)
        UserModel.validate_user_password(data)
        
        data = UserModel.format_user_name(data)

        """
            Input the id value to the
            data dictionary.
        """
        data = UserModel.input_id_values(data)
        
        """
            Creating the user object with the 
            validated and filtered data.
        """
        user: UserModel = UserModel(**data)

        """
            Creating a dictionary with the values
            of the default account
        """
        data_account: dict = {
            'name': 'default',
            'balance': 0.0,
            'user_email': data['email']
        }
        data_account = AccountModel.input_id(data_account)

        """
            Creating the account object with the 
            default data.
        """
        account: AccountModel = AccountModel(**data_account)
        
        """
            Adding the user and account objects to 
            the database and commiting the session.
        """
        session.add(user)        
        session.commit()
        session.add(account)
        session.commit()

        """
            Returning the user object in a JSON format,
            with the http status code CREATED. 
        """
        return jsonify(user), HTTPStatus.CREATED

    except InvalidFieldType as e:
        return default_exception_return(e)

    except MissingKeyException as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)

    except FormatErrorException as e:
        return default_exception_return(e)

    except ValueAlreadyInUseException as e:
        return default_exception_return(e)

    except InvalidCategoryException as e:
        return default_exception_return(e)

    except InvalidRoleException as e:
        return default_exception_return(e)


def get_all() -> tuple:
    """
        Method that list the users.
        This function will search all the users
        from the UserModel. 
    """
    data: list = UserModel.query.all()
    
    """
        Returning the list of users object in a JSON format,
        with the http status code OK. 
    """
    return jsonify(data), HTTPStatus.OK


def login():
    """
        Method to login a user.
        This method checks if the user exists in the 
        database and if the password is correct, 
        allowing or not allowing the user to login. 
    """

    """
        Get the data provided by the post requisition
        and assimilate to a dicitionary.
    """
    user_data = request.get_json()

    """
        Creates a variable to be used later in the 
        creation of the access token, to increase 
        its expiration time to 1 day.
    """
    expire_time: timedelta = timedelta(1)

    """
        Get the user in the database using email 
        as filter.
    """
    found_user: UserModel = UserModel.query.filter_by(email=user_data["email"]).first()

    """
        If the user is not found, the request is closed.
    """
    if not found_user:
        return jsonify({
            "message": "User not found"
        }), HTTPStatus.NOT_FOUND

    """
        If the user is found, the verify_password method 
        is executed. If the password is correct, the 
        login is successful creating an access token, 
        otherwise the user is not authorized.
    """
    if found_user.verify_password(user_data["password"]):
        access_token = create_access_token(identity=loads(jsonify(found_user).response[0].decode()), expires_delta=expire_time)
        return jsonify({
            "token": access_token
        }), HTTPStatus.OK
    else:
        return jsonify({
            "message": "Unauthorized"
        }), HTTPStatus.UNAUTHORIZED


def recover_password():
    """
        Method to send email to reset password.
        This method checks if the user exists in the 
        database, then send a email with the link to reset password. 
    """

    if request.method == 'GET':
        return render_template('reset.html')

    if request.method == 'POST':

        """
            Get the email from the request,
            and then the user by virify_email that says
            if the email exist in database and return the user.
        """
        email = request.form.get('email')
        user = UserModel.verify_email(email)

        if user:
            """
                If the user was found, send his and email
                with the link to chance his password.
            """
            send_email(user)
            return jsonify({
                "message": "Message sent."
                }), HTTPStatus.OK

        else:
            """
                If the user is not found, the request is closed.
            """
            return jsonify({
                "message": "Email not found."
                }), HTTPStatus.NOT_FOUND
      

def verify_reset_token():
    """
        Method to reset the password.
        This method checks if the user exists in the 
        database, then reset password. 
    """
    token = request.args.get("token")
    user = UserModel.verify_reset_token(token)
    """
        If the user is not found, the request is closed.
    """
    if not user:
        return jsonify({
            "message": "User not found"
        }), HTTPStatus.NOT_FOUND

    """
        If the user exists, take the password
        and set for a new password. 
    """
    password = request.form.get('password')
    if password:
        user.set_password(password, commit=True)
        """
            Returning password updated
            with the http status code OK. 
        """
        return jsonify({
            "message": "password updated"
            }), HTTPStatus.OK

    return render_template('reset_verified.html')


def allowed_file(filename):
    """
        Method to verify the extensions of 
        the file that will be uploaded. 
    """
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def upload_file():
    """
        Method to make upload
        of a file. 
    """
    if request.method == 'POST':
        file = request.files['file']
        """
            Verify the extension
            of a file. 
        """
        if file and allowed_file(file.filename):
            file.save(secure_filename(f'app/static/{file.filename}'))
            """
                Returning a message if the file was
                successfully uploaded with the http status code OK. 
            """
            return jsonify({
                "message": "File uploaded successfully'"
                }), HTTPStatus.OK
        """
            If the extension is not allowed,
            the request is closed.
        """
        return jsonify({
            "message": "Extension not allowed."
            }), HTTPStatus.NOT_ACCEPTABLE

    else:
        return render_template('upload.html')


def update_user():
    """
        Method that update an user.
    """

    try:

        """
            Get the data provided by the post requisition
            and assimilate to a list.
        """
        data: dict = request.get_json()    

        """    
            Use the UserModel class methods to check
            the attributes given by the request.
        """
        UserModel.check_update_keys(data)

        user_email: str = data['email']

        """
            Get the user in the database using email 
            as filter.
        """
        user = UserModel.query.filter_by(email=user_email).first()

        if user == None:
            """
                If the user is not found, the request is closed.
            """
            return jsonify({
                "message": "User not found."
            }), HTTPStatus.NOT_FOUND

        else:
            """
                Update the user with
                the request.
            """ 
            user = UserModel.query.filter_by(email=data['email']).update(data)   
            """
                Get the new user in the database using email 
                as filter.
            """ 
            user = UserModel.query.filter_by(email=data['email']).first()
        
        """
            Returning the user object in a JSON format,
            with the http status code CREATED. 
        """
        return jsonify(user), HTTPStatus.OK

    except MissingKeyException as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)

    except InvalidTotalOfKeysException as e:
        return default_exception_return(e)


def delete_user():

    """
        Method that deletes an user.
    """

    try:

        """
            Get the data provided by the post requisition
            and assimilate to a list.
        """
        data: dict = request.get_json()

        """    
            Use the UserModel class methods to check
            the attributes given by the request.
        """
        UserModel.check_delete_keys(data)

        """
            Get the user in the database using email 
            as filter.
        """
        user = UserModel.query.filter_by(email=data['email']).first()

        if user == None:
            """
                If the user is not found, the request is closed.
            """
            return jsonify({
                "message": "User not found."
            }), HTTPStatus.NOT_FOUND

        else:
            """
                Delete the user
                and commit the changes.
            """ 
            current_app.db.session.delete(user)
            current_app.db.session.commit()

        """
            Returning the user object in a JSON format,
            with the http status code CREATED. 
        """
        return jsonify(user), HTTPStatus.OK

    except MissingKeyException as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)
