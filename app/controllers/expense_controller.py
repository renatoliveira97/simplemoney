from flask import jsonify, request, current_app
from flask_jwt_extended.utils import get_jwt_identity
from app.models.account_model import AccountModel
from app.models.expense_model import ExpenseModel
from app.models.user_model import UserModel
from . import default_exception_return
from flask_jwt_extended import jwt_required
from http import HTTPStatus
from app.exceptions.expense_exceptions import (
    InvalidCategoryException    
)
from app.exceptions.user_exceptions import (
    NotFoundUserError
)
from app.exceptions.public_exceptions import (
    InvalidFieldType,
    InvalidKeyException,
    InvalidTypeException,
    InvalidValueException,
    MissingKeyException
)


@jwt_required
def register_expense() -> tuple:

    """
        Method that creates an expense.
        This function will manipulate and validate the data 
        provided by the request, to ensure the object 
        integrity.

        Return
        ------
        Response : `tuple`
             
    """
    try:

        """
            Assimilate the SQLAlchemy session to a
            variable.
        """
        session = current_app.db.session

        """
            Get the data provided by the post requisition
            and assimilate to a dictionary.
        """
        data: dict = request.get_json()

        """
            Use the UserModel class methods to check
            the attributes given by the request.
        """
        ExpenseModel.check_required_keys(data)
        ExpenseModel.check_key_type(data)
        ExpenseModel.check_values(data)

        """
            Input the id and user_email values to the
            data dictionary.
        """
        data = ExpenseModel.input_values(data)

        """
            Creating the expense object with the 
            validated and filtered data.
        """
        expense: ExpenseModel = ExpenseModel(**data)

        """
            Adding the expense object to the database
            and commiting the session.
        """
        session.add(expense)

        user_email: str = data['user_email']
        account_id: str = data['account_id']

        value: float = ExpenseModel.get_value(data)

        actual_user: UserModel = UserModel.query.filter_by(email = user_email).first()
        account: AccountModel = AccountModel.query.filter_by(id = account_id).first()

        balance: float = actual_user.balance
        account_balance: float = account.balance

        new_balance: float = balance + value
        new_account_balance: float = account_balance + value

        balance_dict: dict = {
            "balance": new_balance
        }

        account_balance_dict: dict = {
            "balance": new_account_balance
        }

        """
            Updating user and account balance with the 
            value entered
        """
        UserModel.query.filter_by(email = user_email).update(balance_dict)
        AccountModel.query.filter_by(id = account_id).update(account_balance_dict)
        session.commit()

        """
            Returning the expense object in a Json 
            format, with the http status code CREATED.
        """
        return jsonify(expense), HTTPStatus.CREATED

    except MissingKeyException as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)

    except InvalidFieldType as e:
        return default_exception_return(e)

    except InvalidTypeException as e:
        return default_exception_return(e)

    except InvalidCategoryException as e:
        return default_exception_return(e)

    except InvalidValueException as e:
        return default_exception_return(e)


def read_expenses_by_user_email(user_email: str) -> tuple:

    """
        Function to read the expenses of a specific 
        user.
        The users are filtered by their emails.

        Parameters
        ----------
        user_email: `str`
            The user email

        Return
        ------
            Response : `tuple`
    """
    try:

        """
            Checking if the provided email returns
            a user. If not, this will raise an error.
        """
        UserModel.check_user_email(user_email)

        """
            Getting the list of all expenses of the
            selected user.
        """
        expenses: list = ExpenseModel.query.filter_by(user_email=user_email).all()

        """
            Returning the expenses list in a Json
            format, with the http status code OK.
        """
        return jsonify(expenses), HTTPStatus.OK

    except NotFoundUserError as e:
        return default_exception_return(e)
    