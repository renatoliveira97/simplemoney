from flask.json import jsonify
import pandas as pd
import re
from pandas.core.base import DataError
from pandas.core.frame import DataFrame
import requests
from bs4 import BeautifulSoup
from dotenv import load_dotenv
import yfinance as yf
import json
from app.exceptions.finance_stock_exception import (
    CompanyNotFound,
    ConnectionFailException
)

load_dotenv()

def get_htlm_content(url: str) -> BeautifulSoup:
    """
        Method to extract html from a page by searching by url

        Parameters
        ---------- 
        url : `str`
            A string representing the target page to extract the content

        Raises
        ------
        ConectionFailException
            An error if the connection to the page fails 
    """

    """
        Requesting HTML Content 
    """
    html = requests.get(url)

    """
        checking page connection failure 
    """
    if html.status_code != 200:
        raise ConnectionFailException("Connection with htlm to scrap failed")
    else:
        html_content = html.content

    """
        Creating an instance of class BeautifulSoup
    """
    soup = BeautifulSoup(html_content, 'html.parser')
    
    return soup


def search_in_html(soup: BeautifulSoup, tag: str, identifier: str, desc: str) -> list:
    """
        Method to search for html content coming from
        an instance of BeautifulSoup Class

        Parameters
        ---------- 
        soup : `object`
            A representation of the page on which to search.
            It must be an intancy of BeautifulSoup class
        
        tag : `str`
            A string with the name of the tag
            that will be searched for in the html content 
        
        identifier : `str`
            A string with the tag identifier type.
            Example: class or id.

        desc : `str`
            Description of the tag identifier 
    """

    """
        Search the content by tag and its description 
    """
    content_foud = soup.findAll(tag, {identifier: desc})
    
    return content_foud


def get_top_finance_stock_table(url: str) -> dict:
    """
        Method to create a daily history dictionary for the main companies. 
        **Attention**: Content must have been extracted from the <table> tag 

        Parameters
        ---------- 
        url : `str`
            url string of the table that contains all company symbols 
    """


    most_actives_content = get_htlm_content(url)
    most_actives_content_table = search_in_html(most_actives_content, "table", "class", "W(100%)")

    """
        Creating a new table and separating its contents 
    """
    new_table = most_actives_content_table[0]

    body = new_table.find_all("tr")
    head = body[0] 
    body_rows = body[1:] 

    """
        Iterating over table contents to find header values 
    """
    headings = []
    for item in head.find_all("th"):
        item = (item.text).rstrip("\n")
        headings.append(item)

    """
        Iterating over table contents to find row values according to their header 
    """
    all_rows = [] 
    for row_num in range(len(body_rows)):
        row = []
        for row_item in body_rows[row_num].find_all("td"):
            row_value = re.sub("(\xa0)|(\n)|,","",row_item.text)
            row.append(row_value)
        all_rows.append(row)

    """
        Creating a dataframe for better visualization of the table 
    """
    new_dataframe = pd.DataFrame(data=all_rows,columns=headings)
    symbols_lit = new_dataframe['Símbolo'].to_list()

    """
        creating the feedback dictionary with the main information of the companies on the day 
    """
    companies_history_on_the_day  = {}

    for symbol in symbols_lit:
        company_symbol = yf.Ticker(symbol)
        hist = company_symbol.history(period="1d")
        companies_history_on_the_day[symbol] = json.loads(hist.to_json())

    return companies_history_on_the_day



def get_all_stocks() -> list:
    """
        Method to create a list of
        all stocks available on the b3 website
    """

    """
        Taking all the data from the page and creating
        a list from the however of the "tds" with the
        class 'strong' that are destined to the name of companies
    """
    site = get_htlm_content("https://www.infomoney.com.br/cotacoes/empresas-b3/")
    big_list = search_in_html(site, "td", "class", "strong")


    """
        Using a for loop to extract just the contents
        of the 'td' tag and form a list of all
    """
    all_companies = [] 
    for company in big_list:
        company_symbol = re.sub("(\xa0)|(\n)|,","",company.text)
        all_companies.append(company_symbol)

    return jsonify(all_companies)


def get_stock_by_symbol(symbol) -> DataFrame:
    """
        Method to create a ticket instance using
        yfinance and return your one day history
         
        Parameters
        ---------- 
        symbol : `str`
            Company symbol used to create ticket instance

        Raises
        ------
        CompanyNotFound:
            Error if the company symbol is wrong
            and an empty instance is created
    """

    """
        Removing spaces, making sure the symbol
        ends with F and adding .SA to create the instance
    """
    symbol.replace(" ","")

    if symbol[-1] == "F":
        symbol = symbol[:-1]

    if symbol[-3:] != ".SA":
        symbol += ".SA"
    
    """
        Creating the ticket instance to represent the company 
    """
    company = yf.Ticker(symbol)
    company_hist = company.history(period="1d")
    
    """
        Checking if the instance is valid,
        if it is not a not found error is raised
    """
    if len(company_hist) == 0:
        raise CompanyNotFound("company not found")

    return company_hist.to_json()
