from flask import jsonify

from bs4 import BeautifulSoup
import requests

import pandas as pd

import os


def get_htlm_content(url: str) -> BeautifulSoup:
    """
        Method to extract html from a page by searching by url

        Parameters
        ---------- 
        url : `str`
            A string representing the target page to extract the content

        Raises
        ------
        ConectionFailException
            An error if the connection to the page fails 
    """

    """
        Requesting HTML Content 
    """

    html = requests.get(url)

    """
        checking page connection failure 
    """
    if html.status_code != 200:
        raise
        # raise PageNotFound("Page not Found!")
    else:
        html_content = html.content

    """
        Creating an instance of class BeautifulSoup
    """
    soup = BeautifulSoup(html_content, 'html.parser')

    return soup

def search_in_html(soup: BeautifulSoup, tag: str, identifier: str, desc: str) -> list:
    """
        Method to search for html content coming from
        an instance of BeautifulSoup Class

        Parameters
        ---------- 
        soup : `object`
            A representation of the page on which to search.
            It must be an intancy of BeautifulSoup class
        
        tag : `str`
            A string with the name of the tag
            that will be searched for in the html content 
        
        identifier : `str`
            A string with the tag identifier type.
            Example: class or id.

        desc : `str`
            Description of the tag identifier 
    """

    """
        Search the content by tag and its description 
    """
    content_foud = soup.findAll(tag, {identifier: desc})
    
    return content_foud

def get_all_companies() -> list:
    """
        Method for listing all companies listed on a given site 
    """

    """
        Getting HTML content from the page 
    """
    soup = get_htlm_content(os.environ.get("URL_B3_COMPANIES"))

    """
        Getting content from the "dt" tag 
    """
    content = search_in_html(soup, tag="td", identifier="class", desc="higher")
    
    """
        Creating a dataframe with companies 
    """
    companies = pd.DataFrame(content)

    """
        Creating a list with the values present in the dataframe 
    """
    new_companies = []
    for _, company in companies.iterrows():
        new_companies.append(company[0])

    return jsonify(new_companies)