from http import HTTPStatus
from flask import jsonify, request, current_app
from flask_jwt_extended import jwt_required
from app.models.account_model import AccountModel
from app.controllers import default_exception_return
from app.exceptions.public_exceptions import (
    MissingKeyException,
    InvalidKeyException,
    InvalidFieldType
)
from app.exceptions.account_exceptions import (
    AccountAlreadyExists
)


@jwt_required
def get_account_by_user_email(user_email: str) -> tuple:
    """
        Method to get the information of a specific account.

        **User authentication required**
        
        Parameters
        ----------
        user_email: `str`
            String with the email of the account that will be get

        Raises
        ------
        MissingKeyException:
            Exception if a key is missing
        
        InvalidKeyException:
            Exception if the key is invalid 
    """

    """
        Getting the account in the database with an error handling 
    """
    try:
        
        accounts: list = AccountModel.query.filter_by(user_email = user_email).all()

        if accounts is not None:
            return jsonify(accounts), HTTPStatus.OK
        else:
            return jsonify({
                'message': 'Empty list'
            }), HTTPStatus.NOT_FOUND

    except MissingKeyException as e:
        default_exception_return(e)

    except InvalidKeyException as e:
        default_exception_return(e)


@jwt_required
def create_account() -> tuple:
    """
        Method for creating a new account
        The parameters for creation are received by request
        
        **User authentication required**
        
        Raises
        ------
        MissingKeyException:
            Exception if a key is missing
        
        InvalidKeyException:
            Exception if the key is invalid 

        InvalidFieldType:
            exception if the value of the fields is invalid
        
        AccountAlreadyExists     :
            Exception if it is being created already exists    
    """


    try:
        """
            Starting section with database 
        """
        session = current_app.db.session

        """
            Getting parameters by request
        """
        data: dict = request.get_json()

        """
            Validating request data 
        """
        AccountModel.check_keys(data)
        AccountModel.check_values(data)

        """
            Adding a valid account ID and email address 
        """
        data = AccountModel.input_values(data)

        account: AccountModel = AccountModel(**data)
        
        """
            Adding data and ending database connection 
        """
        session.add(account)
        session.commit()

        return jsonify(account), HTTPStatus.CREATED

    except MissingKeyException as e:
        default_exception_return(e)

    except InvalidKeyException as e:
        default_exception_return(e)

    except InvalidFieldType as e:
        default_exception_return(e)

    except AccountAlreadyExists as e:
        default_exception_return(e)


@jwt_required
def delete_account() -> tuple:
    """
        Method for deleting an account
        Parameters are received by request 

        **User authentication required**

        Raises
        ------
        MissingKeyException:
            Exception if a key is missing
        
        InvalidKeyException:
            Exception if the key is invalid 
    """
    try:
        """
            Starting section with database 
        """
        session = current_app.db.session

        """
            Getting parameters by request
        """
        data: dict = request.get_json()

        """
            Using the class method for key validation 
        """
        AccountModel.check_delete_key(data)

        """
            Using the user id to get it from the database 
        """
        account_id: str = data['id']
        account: AccountModel = AccountModel.query.filter_by(id = account_id).first()

        """
            Deleting account and ending connection 
        """
        session.delete(account)
        session.commit()

        return jsonify(account), HTTPStatus.OK

    except MissingKeyException as e:
        default_exception_return(e)

    except InvalidKeyException as e:
        default_exception_return(e)
