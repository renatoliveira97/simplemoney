from flask import jsonify

def default_exception_return(e):

    return jsonify({
            "error": e.get_message()
        }), e.get_status_code()
        