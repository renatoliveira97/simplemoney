from . import default_exception_return
from flask import json, request, jsonify, current_app
from flask_jwt_extended import jwt_required
from app.models.hint_model import HintModel
from http import HTTPStatus
from app.exceptions.public_exceptions import (
    MissingKeyException,
    InvalidValueException,
    InvalidKeyException
)
from app.exceptions.hint_exception import (
    HintNotFoundException
)


@jwt_required
def get_user_hints(email: str) -> tuple:

    data: list = HintModel.query.filter_by(user_email = email).all()

    return jsonify(data), HTTPStatus.OK


@jwt_required
def generate_a_default_hint() -> tuple:

    try:

        session = current_app.db.session

        data: dict = request.get_json()

        HintModel.check_default_keys(data)

        HintModel.check_default_values(data)

        hint_title: str = data['title']

        data = HintModel.input_default_values(data, hint_title)

        hint: HintModel = HintModel(**data)

        session.add(hint)
        session.commit()

        return jsonify(hint), HTTPStatus.CREATED

    except MissingKeyException as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)

    except InvalidValueException as e:
        return default_exception_return(e)


@jwt_required
def generate_a_custom_hint() -> tuple:

    try:

        session = current_app.db.session

        data: dict = request.get_json()

        HintModel.check_custom_keys(data)

        HintModel.check_custom_values(data)

        data = HintModel.input_id_value(data)

        hint: HintModel = HintModel(**data)

        session.add(hint)
        session.commit()

        return jsonify(hint), HTTPStatus.CREATED

    except MissingKeyException as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)

    except InvalidValueException as e:
        return default_exception_return(e)


@jwt_required
def update_hint() -> tuple:

    try:

        session = current_app.db.session

        data: dict = request.get_json()

        HintModel.check_update_keys(data)

        HintModel.check_update_values(data)

        hint: dict = HintModel.get_hint_to_update(data)

        hint_id: str = hint.id

        HintModel.query.filter_by(id = hint_id).update(data)
        session.commit()

        updated_hint: HintModel = HintModel.query.filter_by(id = hint_id).first()

        return jsonify(updated_hint), HTTPStatus.OK

    except MissingKeyException as e:
        return default_exception_return(e)

    except InvalidKeyException as e:
        return default_exception_return(e)

    except InvalidValueException as e:
        return default_exception_return(e)

    except HintNotFoundException as e:
        return default_exception_return(e)
