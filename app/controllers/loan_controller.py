from flask import request, jsonify, current_app
from app.exceptions.public_exceptions import (
    InvalidFieldType,
    InvalidKeyException, 
    MissingKeyException,
    InvalidTypeException,
    InvalidValueException
)
from app.models.loan_model import LoanModel
from http import HTTPStatus
import numpy as np
from . import default_exception_return


def create_loan():
    """
        Method that creates a loan simulation
    """
    
    try:

        """
            Assimilate the SQLAlchemy session to a
            variable
        """
        session = current_app.db.session

        """
            Get the data provided by the post requisition
            and assimilate to a dictionary
        """
        data: dict = request.get_json()

        """
            Use the LoanModel class methods to check
            the attributes given by the request
        """
        LoanModel.check_required_keys(data)
        LoanModel.check_key_type(data)
        LoanModel.check_values(data)

        """
            Assimilate the values from data dicitionary 
            to variables
        """
        loan_amount = data['loan_amount']
        loan_term = data['loan_term']
        loan_type = data['loan_type'].lower()
        interest_rate = data['interest_rate'] / 100

        balances_float: list = []
        balances: list = []
        amortizations: list = []
        interests: list = []
        payments: list = []

        """
            Loan simulation calculations by loan type
        """
        if loan_type == 'price':
            for i in range(loan_term + 1):
                if i == 0:
                    payments.append('')
                    amortizations.append('')
                    interests.append('')
                    balances.append(f'{loan_amount:.2f}')
                    balances_float.append(loan_amount)
                else:
                    """
                        Calculating payment
                    """
                    payment: float = loan_amount / ((np.power(1 + interest_rate, loan_term) - 1) / (interest_rate * np.power(1 + interest_rate, loan_term)))
                    payments.append(f'{payment:.2f}')
                    
                    """
                        Calculating interest
                    """
                    interest: float = interest_rate * balances_float[i - 1]
                    interests.append(f'{interest:.2f}')
                    
                    """
                        Calculating amortization
                    """
                    amortization: float = payment - interest
                    amortizations.append(f'{amortization:.2f}')
                    
                    """
                        Calculating debit balance 
                    """
                    balance: float = balances_float[i - 1] - amortization
                    balances_float.append(balance)
                    balances.append(f'{balance:.2f}')
        elif loan_type == 'sac':
            for i in range(loan_term + 1):
                if i == 0:
                    payments.append('')
                    amortizations.append('')
                    interests.append('')
                    balances.append(f'{loan_amount:.2f}')
                    balances_float.append(loan_amount)
                else:
                    """
                        Calculating amortization
                    """
                    amortization: float = loan_amount / loan_term
                    amortizations.append(f'{amortization:.2f}')
                    
                    """
                        Calculating interest
                    """
                    interest: float = interest_rate * float(balances_float[i - 1])
                    interests.append(f'{interest:.2f}')
                    
                    """
                        Calculating payment
                    """
                    payment: float = amortization + interest
                    payments.append(f'{payment:.2f}')
                    
                    """
                        Calculating debit balance
                    """
                    balance: float = balances_float[i - 1] - amortization
                    balances_float.append(balance)
                    balances.append(f'{balance:.2f}') 

        """
            Creates a dictionary to pass as an argument to create a 
            loan object
        """
        data_to_model = {
            'payments': payments,
            'amortizations': amortizations,
            'interests': interests,
            'balances': balances,
        }

        """
            Input the id value to the
            data_to_model dictionary
        """
        data_to_model = LoanModel.input_id_values(data_to_model)

        """
            Creating the loan object with the 
            validated and filtered data.
        """
        loan = LoanModel(**data_to_model)

        """
            Adding the expense object to the database
            and commiting the session.
        """
        session.add(loan)
        session.commit()
    
    except InvalidFieldType as e:
        return default_exception_return(e)

    except MissingKeyException as e:
        return default_exception_return(e)
    
    except InvalidKeyException as e:
        return default_exception_return(e)

    except InvalidTypeException as e:
        return default_exception_return(e)

    except InvalidValueException as e:
        return default_exception_return(e)

    return jsonify(loan), HTTPStatus.CREATED
