from flask import jsonify, request, current_app
from flask_jwt_extended import jwt_required
from app.controllers import default_exception_return
from app.exceptions.category_exceptions import (
    CategoryNotFoundException
)
from app.exceptions.public_exceptions import (
    InvalidKeyException,
    MissingKeyException
)

from app.models.category_model import CategoryModel

from http import HTTPStatus


@jwt_required
def get_all_categories():

    """
        Assimilating all the categories
        to the the 'categories' variable.
    """
    categories: CategoryModel = CategoryModel.query.all()

    """
        Returning all the categories in a JSON
        format, with the http status code OK.
    """
    return jsonify(categories), HTTPStatus.OK


@jwt_required
def get_category_by_name(category_name):
    
    try:

        """
            Using the CategoryModel method to check
            if the category exists, if it doesn't,
            it will raise an exception.
        """
        CategoryModel.check_category_exist(category_name)

        """
            Getting the category inside the database, and
            assimilating it to the 'category' variable.
        """
        category: CategoryModel = CategoryModel.query.get(category_name)

        """
            Return the category object in a JSON
            format, with the http status code OK.
        """
        return jsonify(category), HTTPStatus.OK
    except CategoryNotFoundException as e:
        """
            Exception to a category that doesn't exist.
        """
        return default_exception_return(e)


@jwt_required
def create_category():

    """
        This function will create a new
        category, ensuring that the data
        provided by the request is valid.
    """
    try:

        """
            Assimilating the SQLAlchemy session to a
            variable.
        """
        session = current_app.db.session

        """
            Assimilating the data provided by
            the request to the 'data' variable.
        """
        data: dict = request.get_json()
        
        """
            Using the CategoryModel class methods to check
            the attributes given by the request.
        """
        CategoryModel.check_required_keys(data)

        """
            Creating the category object using
            the CategoryModel.
        """
        category: CategoryModel = CategoryModel(**data)

        """
            Adding the category object to the database
            and commiting the session.
        """
        session.add(category)
        session.commit()

        """
            Returning the category in a JSON format,
            with the http status code CREATED.
        """
        return jsonify(category), HTTPStatus.CREATED

    except MissingKeyException as e:
        """
            Exception for keys that are missing.
        """
        return default_exception_return(e)

    except InvalidKeyException as e:
        """
            Exception for keys that are invalid.
        """
        return default_exception_return(e)


@jwt_required
def update_category_description(category_name: str):

    """
        This function allows the user
        to update the category, changing
        its description.
    """
    try:

        """
            Assimilating the SQLAlchemy session to a
            variable.
        """
        session = current_app.db.session
        
        """
            Checking if the category name
            exists inside the database.
        """
        CategoryModel.check_category_exist(category_name)

        """
            Assimilating the data provided
            by the request to the 'data' variable.
        """
        data: dict = request.get_json()

        """
            Checking if the data provided
            by the request are correct.
        """
        CategoryModel.check_required_keys(data)

        category_name: str = data['name']

        """
            Getting the category inside the database, 
            updating its description, and commiting.
        """        
        category: CategoryModel = CategoryModel.query.filter_by(name = category_name).update(data)
        session.commit()

        """
            Assimilating the category with the
            new description to a variable.
        """
        category = CategoryModel.query.filter_by(name = category_name).first()

        """
            Returning the category updated in a JSON format, with the http status code OK.
        """
        return jsonify(category), HTTPStatus.OK

    except CategoryNotFoundException as e:
        """
            Exception to a category that doesn't exist.
        """
        return default_exception_return(e)

    except MissingKeyException as e:
        """
            Exception for keys that are missing.
        """
        return default_exception_return(e)

    except InvalidKeyException as e:
        """
            Exception for keys that are invalid.
        """
        return default_exception_return(e)


@jwt_required
def delete_category(category_name: str):
    
    try:

        """
            Assimilating the SQLAlchemy session to a
            variable.
        """
        session = current_app.db.session

        """
            Using the CategoryModel method to check
            if the category exists, if it doesn't,
            it will raise an exception.
        """
        CategoryModel.check_category_exist(category_name)

        """
            Assimilating the category object to
            the 'category' variable.
        """
        category: CategoryModel = CategoryModel.query.filter_by(name=category_name).first()

        """
            Removing the category from the database,
            and commiting.
        """
        session.delete(category)
        session.commit()

        """
            If the request data is all correct,
            this function will have no returning.
        """
        return "", HTTPStatus.NO_CONTENT

    except CategoryNotFoundException as e:
        """
            Exception to a category that doesn't exist.
        """
        return default_exception_return(e)
