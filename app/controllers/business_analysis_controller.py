from flask import jsonify, request, current_app
from json import load
from http import HTTPStatus


def get_business_codes() -> tuple:

    path: str = 'app/static/business.json'

    file = open(path)

    data: list = load(file)

    return jsonify(data), HTTPStatus.OK


def get_business_dre(code: str) -> tuple:

    path: str = f'app/static/business/{code}.json'

    file = open(path)

    dre: list = load(file)

    return jsonify(dre), HTTPStatus.OK
