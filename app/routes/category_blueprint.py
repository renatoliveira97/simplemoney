from flask import Blueprint
from app.controllers.category_controller import get_all_categories, get_category_by_name, create_category, update_category_description, delete_category

bp = Blueprint('bp_categories', __name__, url_prefix='/categories/v1')

bp.get('')(get_all_categories)
bp.get('/<string:category_name>')(get_category_by_name)
bp.post('')(create_category)
bp.patch('/<string:category_name>')(update_category_description)
bp.delete('/<string:category_name>')(delete_category)
