from flask import Blueprint
from app.controllers.user_controller import (
    create_user,
    delete_user, 
    login,
    get_all,
    recover_password,
    verify_reset_token,
    update_user,
    upload_file
)

bp = Blueprint('users_bp', __name__, url_prefix='/users/v1')

bp.post('')(create_user)
bp.get('')(get_all)
bp.post('/login')(login)
bp.route('/password_reset', methods=['GET', 'POST'])(recover_password)
bp.route('/password_reset_verified', methods=['GET', 'POST'])(verify_reset_token)
bp.patch('')(update_user)
bp.delete('')(delete_user)
bp.route('/upload', methods=['GET', 'POST'])(upload_file)
