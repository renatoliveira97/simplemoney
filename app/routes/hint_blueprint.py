from flask import Blueprint
from app.controllers.hint_controller import (
    get_user_hints,
    generate_a_default_hint,
    generate_a_custom_hint,
    update_hint
)

bp = Blueprint('bp_hints', __name__, url_prefix='/hints/v1')

bp.get('/<email>')(get_user_hints)
bp.post('/default')(generate_a_default_hint)
bp.post('/custom')(generate_a_custom_hint)
bp.patch('')(update_hint)
