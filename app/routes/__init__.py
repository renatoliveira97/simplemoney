from flask import Flask
from . import (
    users_blueprint, 
    expense_blueprint, 
    category_blueprint,
    account_blueprint,
    hint_blueprint,
    portfolio_blueprint,
    loan_blueprint,
    finance_stock_blueprint,
    business_analysis_blueprint
)


def init_app(app: Flask):

    app.register_blueprint(users_blueprint.bp)
    app.register_blueprint(expense_blueprint.bp)
    app.register_blueprint(category_blueprint.bp)
    app.register_blueprint(portfolio_blueprint.bp)
    app.register_blueprint(loan_blueprint.bp)
    app.register_blueprint(account_blueprint.bp)
    app.register_blueprint(hint_blueprint.bp)
    app.register_blueprint(finance_stock_blueprint.bp)
    app.register_blueprint(business_analysis_blueprint.bp)
