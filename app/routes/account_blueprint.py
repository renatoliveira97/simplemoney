from flask import Blueprint
from app.controllers.account_controller import (
    get_account_by_user_email,
    create_account,
    delete_account
) 


bp = Blueprint('bp_accounts', __name__, url_prefix='/accounts/v1')

bp.get('<string:user_email>')(get_account_by_user_email)
bp.post('')(create_account)
bp.delete('')(delete_account)
