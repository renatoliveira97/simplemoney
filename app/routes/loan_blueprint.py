from flask import Blueprint
from app.controllers.loan_controller import create_loan

bp = Blueprint('bp_loans', __name__, url_prefix='/loans/v1')

bp.post('')(create_loan)