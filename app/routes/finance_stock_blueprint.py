from flask import Blueprint
from app.controllers.finance_stock_controller import ( get_all_stocks, get_stock_by_symbol )

bp = Blueprint('finance_stock_bp', __name__, url_prefix='/finance-stock/v1')

bp.get('')(get_all_stocks)
bp.get('/<symbol>')(get_stock_by_symbol)
