from flask import Blueprint
from app.controllers.expense_controller import register_expense, read_expenses_by_user_email

bp = Blueprint('bp_expenses', __name__, url_prefix='/expenses/v1')

bp.post('')(register_expense)
bp.get('/<string:user_email>')(read_expenses_by_user_email)
