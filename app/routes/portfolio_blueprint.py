from flask import Blueprint
from app.controllers.portfolio_controller import create_portfolio, delete_portfolio, get_portfolio, update_portfolio

bp = Blueprint('bp_portfolios', __name__, url_prefix='/portfolios/v1')

bp.post('')(create_portfolio)
bp.get('/<string:name>')(get_portfolio)
bp.patch('/<string:name>')(update_portfolio)
bp.delete('/<string:name>')(delete_portfolio)