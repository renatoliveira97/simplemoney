from flask import Blueprint
from app.controllers.business_analysis_controller import (
    get_business_codes,
    get_business_dre
)

bp = Blueprint('bp_business_analysis', __name__, url_prefix='/business/v1')

bp.get('')(get_business_codes)
bp.get('/<code>')(get_business_dre)
