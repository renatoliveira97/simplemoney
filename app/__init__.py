from flask import Flask
from flask_jwt_extended import JWTManager
from app.configs import init_app, database, migration, auth_jwt
from app import routes
from app.configs.email import mail
from flask_cors import CORS

jwt = JWTManager()

def create_app():
    
    app = Flask(__name__)

    init_app(app)
    cors = CORS(app)
    database.init_app(app)
    migration.init_app(app)
    auth_jwt.init_app(app)
    routes.init_app(app)
    jwt.init_app(app)
    mail.init_app(app)
    cors.init_app(app)

    return app
  