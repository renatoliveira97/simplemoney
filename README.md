<div align="center">
    <h1>Simpl&Money</h1>
    <h4>The simpl&money project backend repository<h4>
    <img src="https://raw.githubusercontent.com/andela-mnzomo/project-dream-team-one/master/flask-crud-part-one.jpg" alt="Flask image">
</div>

## Architecture

Directory tree based on the factory design pattern, used to serve multiple packages with a MVC structure.

```
.
├── app
│   ├── configs
│   │   ├── __init__.py
│   │   ├── auth_jwt.py
│   │   ├── database.py
│   │   ├── email.py
│   │   └── migration.py
│   ├── controllers
│   │   ├── __init__.py
│   │   ├── account_controller.py
│   │   ├── b3_companies_controller.py
│   │   ├── business_analysis_controller.py
│   │   ├── category_controller.py
│   │   ├── expense_controller.py
│   │   ├── finance_stock_controller.py
│   │   ├── hint_controller.py
│   │   ├── loan_controller.py
│   │   ├── portfolio_controller.py
│   │   └── user_controller.py
│   ├── exceptions
│   │   ├── __init__.py
│   │   ├── account_exceptions.py
│   │   ├── category_exceptions.py
│   │   ├── expense_exceptions.py
│   │   ├── finance_stock_exception.py
│   │   ├── hint_exception.py
│   │   ├── portfolio_exceptions.py
│   │   ├── public_exceptions.py
│   │   └── users_exceptions.py
│   ├── models
│   │   ├── __init__.py
│   │   ├── account_model.py
│   │   ├── category_model.py
│   │   ├── expense_model.py
│   │   ├── hint_model.py
│   │   ├── loan_model.py
│   │   ├── portfolio_model.py
│   │   └── user_model.py
│   ├── routes
│   │   ├── __init__.py
│   │   ├── account_blueprint.py
│   │   ├── business_analysis_blueprint.py
│   │   ├── category_blueprint.py
│   │   ├── expense_blueprint.py
│   │   ├── finance_stock_blueprint.py
│   │   ├── hint_blueprint.py
│   │   ├── loan_blueprint.py
│   │   ├── portfolio_blueprint.py
│   │   └── users_blueprint.py
│   ├── static
│   │   ├── business
│   │   ├── dictionaries
│   │   │   └── hints.py
│   │   └── business.json
│   ├── templates
│   │   ├── reset_email.html
│   │   ├── reset_verified.html
│   │   ├── reset.html
│   │   └── upload.html
│   └── __init__.py
├── .dockerignore
├── .env.example
├── .gitignore
├── .gitlab-ci.yml
├── docker-compose.yml
├── Dockerfile
├── entrypoint.sh
├── Procfile
├── README.md
└── requirements.txt
```

## Requirements
Install Docker and Docker Compose

- Docker (https://docs.docker.com/engine/install/)
- Docker Compose (https://docs.docker.com/compose/install/)

## Build 
Create customized image
```
docker-compose build
```

## Run locally
Run the environment
```
docker-compose up -d
```

## Stopping the system
Stop the environment
```
docker-compose down
```

## Remarks

This template is developed and tested on

- Python 3.9
- Ubuntu 20.04.3 LTS
