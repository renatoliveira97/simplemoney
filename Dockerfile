#U se slim buster images
FROM python:3.9-slim-buster as builder

# Create user flask
RUN apt-get update && \
    adduser --disabled-password flask

# Define user flask
USER flask

# Set working directory
WORKDIR /home/flask

# Copy the requirements.txt and update pip
COPY --chown=flask:flask ./requirements.txt /home/flask/requirements.txt
ENV PATH="/home/flask/.local/bin:${PATH}"
RUN pip install --user --upgrade pip && pip install --user -r requirements.txt

# Copy all the source code
COPY --chown=flask:flask . /home/flask

# Define environment envs
ENV SQLALCHEMY_DATABASE_URI=${SQLALCHEMY_DATABASE_URI}
ENV SQLALCHEMY_TRACK_MODIFICATIONS=True
ENV MAIL_USERNAME=${MAIL_USERNAME} 
ENV MAIL_PASSWORD=${MAIL_PASSWORD}
ENV MAIL_SERVER=${MAIL_SERVER}
ENV MAIL_PORT=${MAIL_PORT} 
ENV MAIL_USE_SSL=${MAIL_USE_SSL}

# Expose Port
EXPOSE 8000

# Run Flask migration and application
ENTRYPOINT ["/bin/bash","entrypoint.sh"]
